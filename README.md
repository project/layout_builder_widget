CONTENTS OF THIS FILE
---------------------

* Introduction
* Requirements
* Installation
* Configuration
* Maintainers

INTRODUCTION
------------

[Layout Builder Widget](https://www.drupal.org/project/layout_builder_widget)

This module improves the usability of the Layout Builder interface by
integrating it into entity edit forms. By implementing this module,
content editing becomes more efficient and streamlined with the
following enhancements:

Using this module enhances the functionality of the per-entity
layout builder form in order to facilitate standard content editing:

-  The CRUD functionality is now directly accessible from the entity
edit form, eliminating the need to navigate to a separate layout tab.
-  When creating new entities, the Layout Builder UI can be used
without any additional steps.
-  Translation functionality is included, allowing for each
translation to have its own unique set of blocks.


REQUIREMENTS
------------

This module requires no modules outside of Drupal core.

INSTALLATION
------------

Install Layout Builder Widget module as you would normally install a contributed Drupal
module. Visit https://www.drupal.org/node/1897420 for further information.

CONFIGURATION
-------------

1. Enable Layout builder on display page:
  * On manage display page check
    * **Use Layout Builder**
    * **Allow each item to have its layout customized**.
2. Enable layout builder widget on entity form display page.

MAINTAINERS
-----------

* Oleksandr Mokliak (mokys) - https://www.drupal.org/u/mokys
